# The Grime Programming Language

 - [About](README.md)

 - [The Syntax](./syntax/README.md)
	- [Classes Traits and Enums](./syntax/classes-traits-enums.md)

 - [C Interopt](./c-interopt/README.md)
	- [Basics](./c-interopt/basics.md)
